
# from https://github.com/UrsaDK/getopts_long
# shellcheck disable=SC1091
source "${REPODIR}/lib/getopts_long/getopts_long.bash"

# e.g.: ./test.sh --all -bx --file=filepath/name.sh -g otherfile.sh -- eins "zwei drei" vier
cmdLineOptsShort=() ; cmdLineOptsLong=()
cmdLineOptsShort+=('a')  ; cmdLineOptsLong+=('all') # docs for arg
cmdLineOptsShort+=('b')  ; cmdLineOptsLong+=('beta') # docs for arg
cmdLineOptsShort+=('f:') ; cmdLineOptsLong+=('file:') # docs for arg
cmdLineOptsShort+=('g:') ; cmdLineOptsLong+=('gfile:') # docs for arg
cmdLineOptsShort+=('x') # docs for arg

SHORTOPTSPEC=$(printf '%s\n' "$(IFS=''; printf '%s' "${cmdLineOptsShort[*]}")") # concat array elements without space
LONGOPTSPEC="${cmdLineOptsLong[*]}" # concat array elements with space
while getopts_long ":$SHORTOPTSPEC $LONGOPTSPEC" OPTKEY; do
    case ${OPTKEY} in
        'a'|'all')
            echo 'all triggered'
            ;;
        'b'|'beta')
            echo 'beta triggered'
            ;;
        'f'|'file')
            echo "supplied --file ${OPTARG}"
            ;;
        'g'|'gfile')
            echo "supplied --gfile ${OPTARG}"
            ;;
        'x')
            echo 'x triggered'
            ;;
        '?')
            echo "INVALID OPTION: ${OPTARG}" >&2
            exit 1
            ;;
        ':')
            echo "MISSING ARGUMENT for option: ${OPTARG}" >&2
            exit 1
            ;;
        *)
            echo "UNIMPLEMENTED OPTION: ${OPTKEY}" >&2
            exit 1
            ;;
    esac
done

shift $(( OPTIND - 1 ))
[[ "${1}" == "--" ]] && shift

ARGS=( "$@" )