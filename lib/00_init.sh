#!/usr/bin/env bash
# this file is only to be sourced, not executed

# variables to check if already sourced have to be UNIQUE WITHIN THE WHOLE PROJECT!
if [[ ${HAS_ALREADY_BEEN_SOURCED__00_init:=1} = 0 ]]; then return ; fi
HAS_ALREADY_BEEN_SOURCED__00_init=0 # indicate that this file has been sourced

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
REPODIR=${SCRIPTDIR%/*}

cd "${REPODIR}" || exit

dateExe="date"
if [[ $(uname) == "Darwin" ]]; then
 dateExe="gdate"
fi
STARTTIMEMILLIS=$(($($dateExe +'%s%N') / 1000000)) # datetime from nano seconds to in milliseconds
STARTDATE=$($dateExe +'%Y-%m-%d')
STARTTIME=$($dateExe +'%H:%M:%S')


function stripRepoDir() {
    local callingScript
    #echo "DEBUG: ${BASH_SOURCE[*]}"
    #callingScript="${BASH_SOURCE[${#BASH_SOURCE[@]}-1]}" # last element of BASH_SOURCE array
    callingScript="$*"
    callingScript="${callingScript#$REPODIR}" # cut off beginning REPODIR path (if there)
    callingScript="${callingScript#/}" # cut off trailing slash (if any)
    echo -n -e "${callingScript}"
}
if [[ ${BASH_SOURCE[1]:0:1} = '/' ]]; then
    echo "sourcing lib/00_init.sh (because script was executed, not sourced in $(stripRepoDir ${BASH_SOURCE[1]}))"
fi


function timeElapsed() {
    local ms sec min hours elapsedTime
    local msStr secStr minStr hoursStr
    
    elapsedTime=$(( $($dateExe +'%s%N') / 1000000 - STARTTIMEMILLIS ))
    #fake=$(( $((222 * 3600 * 1000)) + $((3 * 60 * 1000)) + $((4 * 1000)) + 876 ))
    #elapsedTime=$(( elapsedTime + fake ))

    ms=$((elapsedTime % 1000))
    msStr=$(printf "%03d" $ms)
    sec=$((elapsedTime / 1000))
    secStr=$(printf "%02d" $((sec % 60)) )
    min=$((sec / 60))
    minStr=$(printf "%02d" $((min % 60)) )
    hours=$((min / 60))
    [[ hours -gt 0 ]]; hasHours=$?

    if [[ $hasHours -eq 0 ]]; then
        printf "%sh:%sm:%ss,%s" "$hours" "$minStr" "$secStr" "$msStr"
    else
        printf "%sm:%ss,%s" "$minStr" "$secStr" "$msStr"
    fi
}

finish() {
    errorcode=$?
    set +eux # turn off flags
    local from
    from=$(stripRepoDir ${BASH_SOURCE[1]})
    if [[ errorcode -eq 0 ]]; then
        INFO "finish() ok! ($from)"
    else
        FATAL "$from finish() abnormaly! (errorcode: $errorcode)"
    fi
    return $errorcode
}


trap finish EXIT
set -e # exit the script on first error (command not returning $?=0)
set -u # errors if an variable is referenced before being set


declare -A LOGLEVELSHASH=( 
 [FATAL]=1  [ERROR]=2  [WARN]=3  [INFO]=4  [DEBUG]=5  [FINE]=6 [FINER]=7 [FINEST]=8 [TRACE]=9 [ALL]=10
)
set +u
if [[ -z $LOGLEVEL ]]; then LOGLEVEL="INFO" ; fi
LOGLEVELINT=${LOGLEVELSHASH[$LOGLEVEL]}
set -u

function LOGPrefix() {
    local logLevel
    local callingScript
    logLevel="$1"
    if [[ -z $logLevel ]]; then logLevel="INFO" ; fi
    callingScript="$(stripRepoDir "${BASH_SOURCE[2]}")"
    printf "$($dateExe +'%H:%M:%S') %5s $(timeElapsed) ${callingScript} |  " ${logLevel}
}
function TRACE() {
    local logLevel="TRACE" ; if [[ ${LOGLEVELSHASH[$logLevel]} -gt $LOGLEVELINT ]]; then return ; fi
    >&2 printf "$($dateExe +'%H:%M:%S') %5s $(timeElapsed) $*\n" "TRACE"
}
function FINEST() {
    local logLevel="FINEST" ; if [[ ${LOGLEVELSHASH[$logLevel]} -gt $LOGLEVELINT ]]; then return ; fi
    >&2 printf "%s\n" "$(LOGPrefix FINEST)$*"
}
function FINER() {
    local logLevel="FINER" ; if [[ ${LOGLEVELSHASH[$logLevel]} -gt $LOGLEVELINT ]]; then return ; fi
    >&2 printf "%s\n" "$(LOGPrefix FINER)$*"
}
function FINE() {
    local logLevel="FINE" ; if [[ ${LOGLEVELSHASH[$logLevel]} -gt $LOGLEVELINT ]]; then return ; fi
    >&2 printf "%s\n" "$(LOGPrefix FINE)$*"
}
function DEBUG() {
    local logLevel="DEBUG" ; if [[ ${LOGLEVELSHASH[$logLevel]} -gt $LOGLEVELINT ]]; then return ; fi
    >&2 printf "%s\n" "$(LOGPrefix DEBUG)$*"
}
function INFO() {
    local logLevel="INFO" ; if [[ ${LOGLEVELSHASH[$logLevel]} -gt $LOGLEVELINT ]]; then return ; fi
    printf "%s\n" "$(LOGPrefix INFO)$*"
}
function WARN() {
    local logLevel="WARN" ; if [[ ${LOGLEVELSHASH[$logLevel]} -gt $LOGLEVELINT ]]; then return ; fi
    printf "%s\n" "$(LOGPrefix WARN)$*"
}
function ERROR() {
    local logLevel="ERROR" ; if [[ ${LOGLEVELSHASH[$logLevel]} -gt $LOGLEVELINT ]]; then return ; fi
    printf "%s\n" "$(LOGPrefix ERROR)$*"
}
function FATAL() {
    local logLevel="FATAL" ; if [[ ${LOGLEVELSHASH[$logLevel]} -gt $LOGLEVELINT ]]; then return ; fi
    printf "%s\n" "$(LOGPrefix FATAL)$*"
}
function setLogLevel() {
    local tmp
    tmp=${LOGLEVELSHASH[$1]}
    if [[ $tmp = '' ]]; then
        WARN "unknown LOGLEVEL '$1'"
    else
        LOGLEVEL=$tmp
        LOGLEVELINT=${LOGLEVELSHASH[$1]}
    fi
}

function sourceAllLibs() {
    for libFile in ${REPODIR}/lib/*.sh; do source $libFile ; done
}

TRACE "SCRIPTDIR: ${SCRIPTDIR}"
TRACE "REPODIR:   ${REPODIR}"

TRACE "sourced lib/00_init.sh now sourcing all other lib/*.sh ..."
sourceAllLibs
TRACE "sourced lib/00_init.sh done."
